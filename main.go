package main

import (
	"log"
	"strings"

	"gitlab.com/Elevarup/go-plugin-uno/foo"
)

// var Bar = &Foo{
//    Laptop: "acer",
//    Age:    2,
// }

var Bar = foo.Foo{
	Laptop: "acer",
	Age:    2,
}

func ChangeText(text string) string {
	log.Println("Bar: ", Bar)
	return strings.ReplaceAll(text, ":uno", "-1-")
}
